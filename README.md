INSTALLATION
============


# create VIM folder if not exists
```
mkdir -p ~/.vim
cd ~/.vim
```
# pull personal settings
```
git clone https://Desprit@bitbucket.org/Desprit/vim.git
cd vim
```
# move content one level backwards
```
find . -maxdepth 1 -exec mv {} .. \;
cd ..
rm -rf vim
```
# pull Vundle package manager`
```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
ln -s /home/desprit/.vim/.vimrc /home/desprit/
vim +PluginInstall
```
# install some more packages
```
sudo apt-get install ack-grep
```
