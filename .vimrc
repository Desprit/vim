" Set Vundle the runtime path to include Vundle and initialize 
set rtp+=~/.vim/bundle/Vundle.vim 
call vundle#begin()

Plugin 'gmarik/Vundle.vim' " let Vundle manage Vundle, required

" === PLUGINS ===
Plugin 'scrooloose/nerdtree' " Project and file navigation 
Plugin 'godlygeek/csapprox' " Makes GVIM themes work in 256color mode
Plugin 'airblade/vim-gitgutter' "Show git diff in sign column
Plugin 'kien/ctrlp.vim' " Full path fuzzy file, buffer etc. finder for Vim
Plugin 'jiangmiao/auto-pairs' " auto close brackets
Plugin 'Lokaltog/vim-easymotion' " navigate the code easily
Plugin 'klen/python-mode'
Plugin 'christoomey/vim-tmux-navigator' " To navigate through tmux windows as they were vim panes
Plugin 'mileszs/ack.vim' " a search tool
Plugin 'wavded/vim-stylus'
Plugin 'leafgarland/typescript-vim'
Plugin 'godlygeek/tabular' " Markdown syntax support
Plugin 'plasticboy/vim-markdown' " Markdown syntax support
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim' " snippets plugin
Plugin 'garbas/vim-snipmate' " snippets plugin
Plugin 'honza/vim-snippets' " snippets plugin
Plugin 'posva/vim-vue' " syntax highlight for vue js components
Plugin 'ekalinin/Dockerfile.vim' " syntax highlight for Dockerfile
Plugin 'fisadev/vim-isort' " sort python imports
Plugin 'elixir-lang/vim-elixir' " elixir, .exs and .eex syntax
Plugin 'MattesGroeger/vim-bookmarks' " Bookmarks

call vundle#end()

" CtrlP plugin
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPLastMode'
let g:ctrlp_custom_ignore = '\v[\/](venv|node_modules|target|dist)|(\.(swp|ico|git|svn))$'

" YCM plugin
let g:ycm_autoclose_preview_window_after_completion=1 " Close preview window after we choose completion 

" Python-mode
let g:pymode_options = 0
let g:pymode_lint_on_write = 0 " Use :PyLint instead of auto spellchecking
let g:pymode_lint_on_fly = 0
let g:pymode_lint_message = 1
let g:pymode_folding = 0 " Disable folding
let g:pymode_rope_vim_completion = 0
let g:pymode_rope_lookup_project = 0
let g:pymode_rope = 0
let g:pymode_trim_whitespaces = 1
let g:python_options_colorcolumn = 0
let g:pymode_quickfix_window = 0
let g:pymode_motion = 0
let g:pymode_run = 1
let g:pymode_run_bind = '<leader>r'
let g:pymode_rope_organize_imports_bind = '<C-c>ro'
let g:pymode_syntax_all = 1
let g:pymode_syntax_print_as_function = 0


" NerdTree plugin 
map <F3> :NERDTreeToggle<CR>
map <M-o> o<C-w>w
let NERDTreeShowLineNumbers=1 " enable line numbers
autocmd FileType nerdtree setlocal relativenumber" " make sure relative line numbers are used
let NERDTreeIgnore=['__pycache__', '.pytest_cache', '\~$', '\.swp$', '.git$', '\.pyc$', '\.coverage$', '\.postcssrc\.js$', '\.babelrc$', '\.editorconfig$', '\.pyo$', '\.class$', 'pip-log\.txt$']
let NERDTreeShowHidden=1

"nnoremap <silent> <F5> :!clear;python %<CR>

" Vim bookmarks
let g:bookmark_no_default_key_mappings = 1
nmap <Leader>i <Plug>BookmarkAnnotate
nmap <Leader>a <Plug>BookmarkShowAll
nmap <Leader>j <Plug>BookmarkNext
nmap <Leader>k <Plug>BookmarkPrev
nmap <Leader>c <Plug>BookmarkClear
nmap <Leader>x <Plug>BookmarkClearAll
nmap <Leader>kk <Plug>BookmarkMoveUp
nmap <Leader>jj <Plug>BookmarkMoveDown
nmap <Leader>g <Plug>BookmarkMoveToLine

" True color support
"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif


" Colors
set t_Co=256
set background=dark
colorscheme badwolf
highlight Normal cterm=NONE term=NONE
set synmaxcol=200 " dont highlight syntax on very long lines



" Spaces and Tabs
autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 
autocmd FileType php setlocal expandtab shiftwidth=4 tabstop=8 
\ formatoptions+=croq softtabstop=4 smartindent
\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with 
autocmd Filetype html setlocal ts=2 sts=2 sw=2
autocmd Filetype vue setlocal ts=2 sts=2 sw=2
autocmd Filetype css,sass setlocal ts=2 sts=2 sw=2
autocmd Filetype scss setlocal ts=2 sts=2 sw=4
autocmd Filetype jinja,jinja2 setlocal ts=2 sts=2 sw=2
autocmd Filetype elixir setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.html,*.htm set ft=html
au BufNewFile,BufRead *.shtml,*.stm,*.jinja,*.jinja2 set ft=html
au BufNewFile,BufRead *.exs,*.ex,*.eex set ft=elixir
au BufNewFile,BufRead *.sh set ft=sh
au BufNewFile,BufRead Dockerfile* set ft=dockerfile
au BufNewFile,BufRead *.js setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.ts setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.less setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.json setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.styl setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.sh setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.less set filetype=less
au BufNewFile,BufRead *.sass set filetype=less
au BufNewFile,BufRead *.ts set filetype=typescript
au BufNewFile,BufRead *.vue set ft=html
au BufNewFile,BufRead *.vue setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.styl set ft=stylus
au BufNewFile,BufRead *.rs setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.rs set ft=rust
au BufNewFile,BufRead *.yml setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead *.bashrc setlocal ts=2 sts=2 sw=2
au BufNewFile,BufRead Dockerfile setlocal ts=2 sts=2 sw=2
set nowrap " Dont break long lines
:inoremap <CR> <CR>x<BS>
set ai
set pastetoggle=<F10> " toggle paste mode with F10. Should be done before pasting code.

" UI layout
set number " show line numbers 
set relativenumber
set nocursorline " highlight current line 
set showmatch " higlight matching parenthesis 
set enc=utf-8
set foldlevel=99
set so=999 " search results appears in the middle of the screen
set guioptions-=T " hide toolbar 
set guioptions-=r " hide scrollbar
:nnoremap <space> i<space><esc>
:set expandtab
autocmd BufEnter * :syntax sync fromstart " always highlight syntax from start
let hlstate=0
nnoremap <F4> :if (hlstate%2 == 0) \| nohlsearch \| else \| set hlsearch \| endif \| let hlstate=hlstate+1<cr>

" Other stuff
let g:netrw_gx="<cword>" " open full url with gx (with arguments after ?)  
autocmd! bufwritepost .vimrc source % " auto update on file save
inoremap jj <ESC>
set wildignore=.svn,CVS,.git,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif,*.pdf,*.bak,*.beam
let g:gitgutter_realtime = 0
let g:gitgutter_max_signs = 5000
set shell=/bin/bash
set backupcopy=yes " correctly trigger webpack updates on vim save file
let mapleader = "`" " replace leader key

" Commenting blocks of code.
vnoremap # :s#^#\# #<cr>
vnoremap -# :s#^\# ##<cr>
vnoremap ? :s?^?\// <cr>
vnoremap -? :s?^\// <cr>

set completeopt-=preview
inoremap <C-e> <C-n>
